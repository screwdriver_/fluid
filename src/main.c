#include "fluid.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * Prints usage for the program.
 */
static void print_help(void)
{
	printf("fluid is a simple Smoothed-Particle Hydrodynamics simulation\n");
	printf("usage: fluid <file>\n");
}

int main(int argc, char **argv)
{
	char *buff;
	simulation_t s;

	if(argc != 2)
	{
		print_help();
		return 0;
	}
	printf("Reading file...\n");
	if(!(buff = read_file(argv[1])))
	{
		dprintf(STDERR_FILENO, "Failed to read file!\n");
		return 1;
	}
	printf("Parsing file...\n");
	bzero(&s, sizeof(simulation_t));
	parse_file(buff, &s);
	free((void *) buff);
	printf("Beginning simulation...\n");
	run_simulation(&s);
	octree_freeall(&s.particles_tree, &free);
	return 0;
}
