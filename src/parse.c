#include "fluid.h"
#include <stdlib.h>

// TODO rm
#include <stdio.h>

void parse_particle(simulation_t *s, char *buff, const size_t len)
{
	char *begin;
	particle_t *p;
	char *end;

	if(!(p = mem_alloc(sizeof(particle_t))))
	{
		// TODO Error
		return;
	}
	begin = buff;
	begin[len] = '\0';
	p->pos.x = strtof(buff, &end);
	buff = end;
	p->pos.y = strtof(buff, &end);
	buff = end;
	p->pos.z = strtof(buff, &end);
	buff = end;
	p->mass = strtof(buff, &end);
	buff = end;
	p->viscosity = strtof(buff, &end);
	begin[len] = '\n';
	p->next = s->particles;
	s->particles = p;
	if(octree_insert(&s->particles_tree, &p->pos, p) < 0)
	{
		// TODO Error
		free((void *) p);
	}
}

void parse_file(char *buff, simulation_t *s)
{
	size_t len, i = 0, l;
	char *end;

	len = strlen(buff);
	while(i < len)
	{
		if(!(end = strchr(buff + i, '\n')) || (l = end - (buff + i)) == 0)
			break;
		if(buff[i] == 'p')
			parse_particle(s, buff + i + 1, l - 1);
		else if(buff[i] == 'c')
		{
			// TODO
		}
		i += l + 1;
	}
}
