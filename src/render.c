#include "fluid.h"
#include "render.h"
#include <stdio.h>
#include <unistd.h>

static shader_t particle_shader;

static void shader_check_error(const int shader, const int type)
{
	int success;
	char info_log[512];

	glGetShaderiv(shader, type, &success);
	if(success)
		return;
	glGetShaderInfoLog(shader, sizeof(info_log), NULL, info_log);
	printf("Shader errors:\n%.*s\n", (int) sizeof(info_log), info_log);
}

// TODO Shorten
static int load_shader(shader_t *shader,
	const char *vertex_path, const char *fragment_path)
{
	// TODO Pass as arguments
	const float vertices[] = {
		0, 0, 0,
		1, 0, 0,
		1, 1, 0,
		0, 1, 0
	};
	const unsigned indices[] = {
		0, 1, 2,
		2, 3, 0
	};
	char *vertex_source, *fragment_source;
	GLuint vertex_shader, fragment_shader;

	glGenVertexArrays(1, &shader->vao);
	glGenBuffers(1, &shader->vbo);
	glGenBuffers(1, &shader->ebo);
	glBindVertexArray(shader->vao);
	glBindBuffer(GL_ARRAY_BUFFER, shader->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), NULL);
	glEnableVertexAttribArray(0);  
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shader->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
		GL_STATIC_DRAW);
	if(!(vertex_source = read_file(vertex_path)))
	{
		dprintf(STDERR_FILENO, "Cannot read file `%s`!\n", vertex_path);
		// TODO Free all
		return 1;
	}
	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1,
		(const GLchar * const*) &vertex_source, NULL);
	glCompileShader(vertex_shader);
	shader_check_error(vertex_shader, GL_COMPILE_STATUS);
	if(!(fragment_source = read_file(fragment_path)))
	{
		dprintf(STDERR_FILENO, "Cannot read file `%s`!\n", fragment_path);
		// TODO Free all
		return 1;
	}
	fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1,
		(const GLchar * const*) &fragment_source, NULL);
	glCompileShader(fragment_shader);
	shader_check_error(fragment_shader, GL_COMPILE_STATUS);
	shader->program = glCreateProgram();
	glAttachShader(shader->program, vertex_shader);
	glAttachShader(shader->program, fragment_shader);
	glLinkProgram(shader->program);
	shader_check_error(shader->program, GL_LINK_STATUS);
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);
	glUseProgram(shader->program);
	return 0;
}

void init_render(void)
{
	load_shader(&particle_shader, "particle.vert", "particle.frag");
	// TODO
}

static void render_particle(const vec_t *pos)
{
	glUseProgram(particle_shader.program);
	glUniform2f(glGetUniformLocation(particle_shader.program, "display_size"),
		WIN_WIDTH, WIN_HEIGHT);
	glUniform2f(glGetUniformLocation(particle_shader.program, "center"),
		0.5f, 0.5f);
	glUniform1f(glGetUniformLocation(particle_shader.program, "radius"), 0.5f);
	// TODO Uniform
	(void) pos;
	glBindVertexArray(particle_shader.vao);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void render(const simulation_t *s)
{
	if(!s)
		return;
	vec_t v = { .x = 0, .y = 0, .z = 0 };
	render_particle(&v);
	// TODO
}
