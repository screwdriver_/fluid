#ifndef RENDER_H
# define RENDER_H

# define GL_GLEXT_PROTOTYPES	1

# include <GLFW/glfw3.h>

typedef struct
{
	GLuint vao, vbo, ebo;
	GLuint program;
} shader_t;

void init_render(void);
void render(const simulation_t *s);

#endif
