#include "fluid.h"
#include <stdio.h>
#include <stdlib.h>

// TODO Handle multiple elements at same position?

/*
 * Returns the octant for the given delta vector.
 */
static unsigned get_child(const vec_t *delta)
{
	unsigned gray_code[3], i;

	gray_code[0] = (delta->x < 0);
	gray_code[1] = (delta->y < 0);
	gray_code[2] = (delta->z < 0);
	i = gray_code[2] ^ gray_code[1];
	i = (gray_code[2] << 2) | (i << 1) | (i ^ gray_code[0]);
	assert(i < 8);
	return i;
}

/*
 * Returns the closest octree node for the specified position.
 * If the object at the position doesn't exist, the node on which the point
 * should be inserted is returned.
 */
static octree_t *octree_get_(octree_t *tree, const vec_t *pos)
{
	vec_t d;
	octree_t *t;

	if(!tree || !pos)
		return NULL;
	t = tree;
	while(t)
	{
		tree = t;
		d.x = pos->x - tree->pos.x;
		d.y = pos->y - tree->pos.y;
		d.z = pos->z - tree->pos.z;
		t = tree->octants[get_child(&d)];
	}
	return tree;
}

/*
 * Returns the octree node for the specified position.
 */
octree_t *octree_get(octree_t *tree, const vec_t *pos)
{
	octree_t *t;

	t = octree_get_(tree, pos);
	if(!t->value)
		return NULL;
	// TODO Use epsilon for comparison
	if(t->pos.x != pos->x || t->pos.y != pos->y || t->pos.z != pos->z)
		return NULL;
	return t;
}

/*
 * Invokes the given function for every values in the octree.
 */
void octree_foreach(octree_t *tree, void (*f)(octree_t *))
{
	size_t i;

	if(!tree || !f)
		return;
	if(tree->value)
	{
		f(tree);
		return;
	}
	for(i = 0; i < 8; ++i)
		octree_foreach(tree->octants[i], f);
}

/*
 * Inserts the given node as child for the given parent node.
 */
static void node_insert(octree_t *parent, octree_t *node)
{
	vec_t d;
	unsigned c;

	node->parent = parent;
	d.x = node->pos.x - parent->pos.x;
	d.y = node->pos.y - parent->pos.y;
	d.z = node->pos.z - parent->pos.z;
	c = get_child(&d);
	parent->octants[c] = node;
	++parent->children_count;
}

/*
 * Inserts a new node in the specified octree at the specified position with the
 * specified value.
 * If an element is already present at the given position, the insertion will
 * fail.
 *
 * If the insertion succeeds, the function returns 0, else -1.
 */
int octree_insert(octree_t **tree, const vec_t *pos, void *value)
{
	octree_t *t, *node, *parent;

	if(!tree || !pos || !value)
		return -1;
	if(!*tree && !(*tree = mem_alloc(sizeof(octree_t))))
		return -1;
	if(!(node = mem_alloc(sizeof(octree_t))))
		return -1;
	node->pos = *pos;
	node->value = value;
	t = octree_get_(*tree, pos);
	if(t->value)
	{
		// TODO Use epsilon for comparison
		if(t->pos.x == pos->x && t->pos.y == pos->y && t->pos.z == pos->z)
		{
			free((void *) node);
			return -1;
		}
		if(!(parent = mem_alloc(sizeof(octree_t))))
		{
			free((void *) node);
			return -1;
		}
		parent->pos.x = (t->pos.x + node->pos.x) / 2;
		parent->pos.y = (t->pos.y + node->pos.y) / 2;
		parent->pos.z = (t->pos.z + node->pos.z) / 2;
		node_insert(t->parent, parent);
		node_insert(parent, t);
		node_insert(parent, node);
	}
	else
		node_insert(t, node);
	return 0;
}

/*
 * Returns the child of the given node. (assuming it has only one)
 */
static octree_t *get_single_child(octree_t *node)
{
	size_t i;

	for(i = 0; i < sizeof(node->octants) / sizeof(*node->octants); ++i)
	{
		if(node->octants[i])
			return node->octants[i];
	}
	return NULL;
}

/*
 * Unlinks the given node from its parent.
 */
static void node_unlink(octree_t *node)
{
	vec_t d;

	d.x = node->pos.x - node->parent->pos.x;
	d.y = node->pos.y - node->parent->pos.y;
	d.z = node->pos.z - node->parent->pos.z;
	node->parent->octants[get_child(&d)] = NULL;
	--node->parent->children_count;
}

/*
 * Removes the specified node from its tree and frees it. The value field isn't
 * freed.
 */
void octree_remove(octree_t **tree, octree_t *node)
{
	octree_t *c;

	// TODO Check that useless nodes are removed
	if(!tree || !node || node->children_count > 1)
		return;
	if(node->children_count == 1)
	{
		c = get_single_child(node);
		memcpy(&node->octants, &c->octants, sizeof(node->octants));
		node->children_count = c->children_count;
		node->pos = c->pos;
		node->value = c->value;
		free((void *) c);
		return;
	}
	else if(node->parent)
	{
		node_unlink(node);
		if(node->parent->children_count == 0)
			octree_remove(tree, node->parent);
	}
	free((void *) node);
	if(node == *tree)
		*tree = NULL;
}

void octree_freeall_(octree_t *tree, void (*f)(void *))
{
	size_t i;

	if(!tree)
		return;
	if(f)
		f(tree->value);
	for(i = 0; i < 8; ++i)
		octree_freeall_(tree->octants[i], f);
	free((void *) tree);
}

/*
 * Frees the given tree. If a function pointer is passed, it will be invoked for
 * every values in the tree.
 */
void octree_freeall(octree_t **tree, void (*f)(void *))
{
	octree_freeall_(*tree, f);
	*tree = NULL;
}

static void octree_print_(octree_t *tree, const size_t level)
{
	size_t i;

	if(!tree)
		return;
	print_vector(&tree->pos);
	printf(" (%s)\n", (tree->value ? "value" : "no value"));
	for(i = 0; i < 8; ++i)
	{
		if(!tree->octants[i])
			continue;
		print_tabs(level + 1);
		printf("%zu: ", i);
		octree_print_(tree->octants[i], level + 1);
	}
}

/*
 * Prints the given octree.
 */
void octree_print(octree_t *tree)
{
	octree_print_(tree, 0);
}
