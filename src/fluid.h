#ifndef FLUID_H
# define FLUID_H

# include <assert.h>
# include <string.h>

# define WIN_WIDTH	1920
# define WIN_HEIGHT	1080

# define PARTICLE_RADIUS	0.25f
# define SMOOTHING_LENGTH	1.f

typedef struct
{
	float x, y, z;
} vec_t;

typedef struct octree
{
	struct octree *parent;
	struct octree *octants[8];
	size_t children_count;

	vec_t pos;
	void *value;
} octree_t;

typedef struct particle
{
	struct particle *next;

	vec_t pos;
	vec_t speed;

	float mass;
	float viscosity;
} particle_t;

typedef struct
{
	particle_t *particles;
	octree_t *particles_tree;

	// TODO
} simulation_t;

void *mem_alloc(size_t n);
char *read_file(const char *file);
void print_tabs(size_t n);
void print_vector(const vec_t *v);

octree_t *octree_get(octree_t *tree, const vec_t *pos);
void octree_foreach(octree_t *tree, void (*f)(octree_t *));
int octree_insert(octree_t **tree, const vec_t *pos, void *value);
void octree_remove(octree_t **tree, octree_t *node);
void octree_freeall(octree_t **tree, void (*f)(void *));
void octree_print(octree_t *tree);

void parse_file(char *buff, simulation_t *s);

void run_simulation(simulation_t *s);

#endif
