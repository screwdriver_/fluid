#include "fluid.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * Allocates the given amount of memory in bytes and returns the pointer to the
 * beginning. This memory can be freed using `free`.
 */
void *mem_alloc(size_t n)
{
	void *ptr;

	if((ptr = malloc(n)))
		bzero(ptr, n); // TODO Check if already done by malloc on every platforms
	return ptr;
}

/*
 * Reads the content of the specified file.
 */
char *read_file(const char *file)
{
	int fd;
	size_t len;
	char *buff;

	if((fd = open(file, O_RDONLY)) < 0)
		return NULL;
	len = lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);
	if(!(buff = mem_alloc(len + 1)))
	{
		close(fd);
		return NULL;
	}
	read(fd, buff, len); // TODO In loop?
	close(fd);
	return buff;
}

/*
 * Prints the given number of tabs.
 */
void print_tabs(size_t n)
{
	while(n--)
		printf("\t");
}

/*
 * Prints the given vector.
 */
void print_vector(const vec_t *v)
{
	printf("[ %f %f %f ]", v->x, v->y, v->z);
}
