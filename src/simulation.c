#include "fluid.h"
#include "render.h"
#include <math.h>

// TODO Handle constraints

/*
 * Computes the Radial Basis Function kernel.
 */
#define COMPUTE_PROPERTY_FUNC(property)\
	float compute_##property(simulation_t *s, const vec_t *pos)\
	{\
		particle_t *p;\
		float n = 0;\
\
		p = s->particles;\
		while(p)\
		{\
			n += sphere_volume(PARTICLE_RADIUS) * p->property\
				* rbf_kernel(pos, &p->pos, SMOOTHING_LENGTH);\
			p = p->next;\
		}\
		return n;\
	}

static GLFWwindow *open_window(void)
{
	GLFWwindow *win;

	if(!glfwInit())
		return NULL;
	if(!(win = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "SPH simulation",
		NULL, NULL)))
	{
		glfwTerminate();
		return NULL;
	}
	glfwMakeContextCurrent(win);
	return win;
}

/*
 * Computes the Radial Basis Function kernel.
 */
static inline float rbf_kernel(const vec_t *n0, const vec_t *n1,
	const float sigma)
{
	vec_t n;
	float m;

	n.x = (n0->x - n1->x);
	n.y = (n0->y - n1->y);
	n.z = (n0->z - n1->z);
	m = n.x * n.x + n.y * n.y + n.z * n.z;
	return exp(-(m * m) / (2.f * sigma * sigma));
}

/*
 * Computes the volume of a sphere of given radius.
 */
static inline float sphere_volume(const float radius)
{
	return (4.f * M_PI * radius * radius * radius) / 3.f;
}

COMPUTE_PROPERTY_FUNC(mass)
COMPUTE_PROPERTY_FUNC(viscosity)

/*
 * Computes the forces applied on the given particle.
 */
static void compute_forces(simulation_t *s, const particle_t *p, vec_t *forces)
{
	// TODO
	(void) s;
	(void) p;
	(void) forces;
	(void) compute_mass;
	(void) compute_viscosity;
}

/*
 * Updates the positions of the particles according to the rules of the
 * simulation and the time-delta.
 */
static void update_particles_position(simulation_t *s, const float delta)
{
	particle_t *p;
	vec_t forces;

	p = s->particles;
	while(p)
	{
		compute_forces(s, p, &forces);
		p->speed.x += forces.x * delta;
		p->speed.y += forces.y * delta;
		p->speed.z += forces.z * delta;
		p->pos.x += p->speed.x * delta;
		p->pos.y += p->speed.y * delta;
		p->pos.z += p->speed.z * delta;
		p = p->next;
	}
}

/*
 * Runs the simulation with the given data.
 */
void run_simulation(simulation_t *s)
{
	GLFWwindow *win;
	float dtime = 1.f, time = 0.f;

	if(!s)
		return;
	if(!(win = open_window()))
		return; // TODO Error
	init_render();
	while(!glfwWindowShouldClose(win))
	{
        glfwPollEvents();
		// TODO Handle mouse/keyboard input (move camera, change simulation speed or gravity)
		update_particles_position(s, dtime);
		glClear(GL_COLOR_BUFFER_BIT);
		render(s);
        glfwSwapBuffers(win);
		time += dtime; // TODO Compute delta between frames
		// TODO Print FPS
	}
	// TODO Free render stuff
	glfwTerminate();
}
