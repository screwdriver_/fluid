#version 330 core

in vec4 gl_FragCoord;
out vec4 FragColor;

uniform vec2 display_size;
uniform vec2 center;
uniform float radius;

void main()
{
	vec4 color = vec4(0.f, 0.f, 1.f, 0.f);
	vec2 coord = (gl_FragCoord.xy / display_size) - center;
	FragColor = color * float(length(coord) <= radius);
}
