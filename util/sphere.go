package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

func printHelp() {
	fmt.Printf("usage: sphere <radius> <mass> <viscosity>\n")
}

func printSphere(radius int, mass float64, viscosity float64) {
	for x := -radius; x < radius; x++ {
		for y := -radius; y < radius; y++ {
			for z := -radius; z < radius; z++ {
				x_ := float64(x)
				y_ := float64(y)
				z_ := float64(z)
				if math.Sqrt(x_ * x_ + y_ * y_ + z_ * z_) <= float64(radius) {
					fmt.Printf("p %v %v %v %v %v\n", x, y, z, mass, viscosity)
				}
			}
		}
	}
}

// TODO Take center x/y/z

func main() {
	if len(os.Args) != 4 {
		printHelp()
		return
	}
	radius, _ := strconv.ParseInt(os.Args[1], 0, 32)
	mass, _ := strconv.ParseFloat(os.Args[2], 64)
	viscosity, _ := strconv.ParseFloat(os.Args[3], 64)
	printSphere(int(radius), mass, viscosity)
}
