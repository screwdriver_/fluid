# fluid

fluid is a simple Smoothed-Particle Hydrodynamics simulation.



## File format

fluid takes one file as input, it contains the list of elements in the simulation.
Here is the declaration of a particle:

```
p <x> <y> <z> <mass> <viscosity>
```

Here is the declaration of a contraint:

```
TODO
```



Example:

```
p 0 0 3 1 1
p 1 3 -4 1 1
p 2 2 -1 1 1
```

TODO: Example of constraints
